import React, { Component } from 'react'
import Painel from './components/Painel'
import Sidebar from './components/Sidebar'
import './css/pure-min.css'
import './css/side-menu.css'

class App extends Component {
	render() {
		return (
			<div id="layout">
				<Sidebar/>
				<Painel title="Painel de Autores" subtitle="cadastro e visualização de autores"/>
			</div>
		)
	}
}

export default App
