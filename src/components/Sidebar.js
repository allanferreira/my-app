import React, { Component } from 'react'
import SidebarItem from './SidebarItem'

export default class Sidebar extends Component {
	render() {
		return (
			<div id="menu">
				<a href="#menu" id="menuLink" className="menu-link">
					<span></span>
				</a>
				<div className="pure-menu">
					<a className="pure-menu-heading" href="#">Dashboard</a>

					<ul className="pure-menu-list">
						<SidebarItem classname="pure-menu-selected" label="Autor"/>
					</ul>
				</div>
			</div>
		)
	}
}