import React, { Component } from 'react'

/* eslint-disable */

export default class SidebarItem extends Component {
	render() {
		return (
			<li className="pure-menu-item" className={this.props.classname}>
				<a href="#" className="pure-menu-link">{this.props.label}</a>
			</li>
		)
	}
}