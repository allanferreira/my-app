import React, { Component } from 'react'
import AutorBox from './AutorBox'

export default class Painel extends Component {
	render() {
		return (
			<div id="main">
				<div className="header">
					<h1>{this.props.title}</h1>
					<h2>{this.props.subtitle}</h2>
				</div>
				<div className="content">
					<AutorBox/>
				</div>
			</div>
		)
	}
}