import React, { Component } from 'react'
export default class InputCustomizado extends Component {
	render(){
		return (
			<div className="pure-control-group">
				<label htmlFor={this.props.name}>{this.props.label}</label> 
				<input 
					className="pure-input-1" 
					id={this.props.id} 
					type={this.props.type} 
					name={this.props.name} 
					value={this.props.value} 
					onChange={this.props.onchange}
				/>
			</div>
		)
	}	
}