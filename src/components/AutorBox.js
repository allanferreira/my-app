import React, { Component } from 'react'
import $ from 'jquery'
import FormularioAutor from './FormularioAutor'
import ListaAutor from './ListaAutor'

export default class AutorBox extends Component {
	constructor(){
		super()
		
		this.state = {
			lista:[]
		}
		
		this.updateLista = this.updateLista.bind(this)
	}
	
	componentDidMount(){
		$.ajax({
			dataType: 'json',
			url: 'http://localhost:5000/api/autores',
			success: (res) => {
				this.setState({ lista: res })
			}
		})
	}

	updateLista(novalista){
		this.setState({ lista: novalista })
	}

	render(){
		return (
			<div className="pure-g">
				{/*<FormularioAutor updateLista={this.updateLista}/>*/}
				<FormularioAutor/>
				<ListaAutor lista={this.state.lista} />
			</div>
		)
	}	
}