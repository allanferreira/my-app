import React, { Component } from 'react'
import $ from 'jquery'
import InputCustomizado from './InputCustomizado'

export default class FormularioAutor extends Component {
	constructor(){
		super()
		
		this.state = {
			nome:'',
			email:'',
			senha:''
		}

		this.setInput = this.setInput.bind(this)
		this.sendForm = this.sendForm.bind(this)
	}
	
	setInput(evento) {
	    const target = evento.target
	    const value  = target.value
	    const name   = target.name

		this.setState({
			[name]: value
		})

		console.log(this.state.nome)
	}

	sendForm(evento){
		evento.preventDefault()
		
		$.ajax({
			contentType: 'application/json',
			dataType: 'json',
			url: 'http://localhost:5000/api/autores',
			type: 'post',
			data: JSON.stringify({
				id:    Math.random().toString(36).substring(7),
				nome:  this.state.nome,
				email: this.state.email,
				senha: this.state.senha
			}),
			cache: false,
			crossDomain: true,
			success: function(res){
				console.log(res)
				//this.props.updateLista(res)
		    }.bind(this),
			error: (err) => {
				console.log(err)
			}
		})
	}

	render(){
		return (
			<div className="pure-u-1-2">
				<h2 className="content-subhead">Insira seu autor</h2>
				<form className="pure-form pure-form-stacked" onSubmit={this.sendForm}>
					<InputCustomizado label="Nome" id="nome" type="text" name="nome" value={this.state.nome} onchange={this.setInput}/>
					<InputCustomizado label="Email" id="email" type="email" name="email" value={this.state.email} onchange={this.setInput}/>
					<InputCustomizado label="Senha" id="senha" type="password" name="senha" value={this.state.senha} onchange={this.setInput}/>

					<div className="pure-control-group">                                  
						<label></label> 
						<input type="submit" className="pure-button pure-button-primary" value="Gravar"/>
					</div>
				</form>
			</div>
		)
	}	
}