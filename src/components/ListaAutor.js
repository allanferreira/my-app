import React, { Component } from 'react'

export default class ListaAutor extends Component {
	render(){
		return (
			<div className="pure-u-1-2">							
				<h2 className="content-subhead">Autores cadastrados</h2>
				<table className="pure-table">
					<thead>
						<tr>
							<th>Nome</th>
							<th>email</th>
						</tr>
					</thead>
					<tbody>
						{
							this.props.lista.map((autor) => {
								return (
									<tr key={autor.id}>
										<td>{autor.nome}</td>
										<td>{autor.email}</td>
									</tr>
								)
							})
						}
					</tbody>
				</table> 
			</div>
		)
	}	
}